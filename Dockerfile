#FROM node
#WORKDIR /app
#COPY package.json .
#RUN npm install 
#RUN npm ci --only=production && npm cache clean --force
#COPY . /app
##CMD node index.js
#CMD npm start
#EXPOSE 8081
#CMD [ "node", "server.js" ]

#WORKDIR /app
#COPY . /app



#FROM node
#ENV NODE_ENV=production

#WORKDIR /app

#COPY ["package.json", "package-lock.json*", "./"]

#RUN npm install --production

#COPY . .

#CMD [ "node", "index.js" ]



FROM node:8.11-alpine

RUN mkdir -p /app/code
WORKDIR /app/code
EXPOSE 8080

COPY ./package.json /app/code
COPY ./package-lock.json /app/code
RUN npm i

COPY . /app/code

CMD node index.js


#CMD export PORT=$PORT && npm start
