#!/bin/bash

docker build -t cowsay:1.0 .

if [ -n "$1" ]; then

	docker run -d -p $1:8080 cowsay:1.0
	else
	docker run -d -p 8080:8080 cowsay:1.0
fi
docker ps

